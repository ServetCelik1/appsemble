import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  title: 'Search',
  placeholder: 'Search…',
});
